package com.example.springmapstructmaven.entity;

import lombok.Data;

@Data
public class Foo {
    private String bar = "";
    private String baz = "";
}
