package com.example.springmapstructmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMapStructMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMapStructMavenApplication.class, args);
	}

}
