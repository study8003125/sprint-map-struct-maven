package com.example.springmapstructmaven.mapper;

import com.example.springmapstructmaven.dto.FooDto;
import com.example.springmapstructmaven.entity.Foo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface IFooMapper {
    IFooMapper INSTANCE = Mappers.getMapper(IFooMapper.class);
    @Mapping(source = "br", target = "bar")
    @Mapping(source = "bz", target = "baz")
    Foo fooDtoToFoo(FooDto dto);

    @Mapping(source = "bar", target = "br")
    @Mapping(source = "baz", target = "bz")
    FooDto fooToFooDto(Foo foo);
}
