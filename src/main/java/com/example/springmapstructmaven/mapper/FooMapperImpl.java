package com.example.springmapstructmaven.mapper;

import com.example.springmapstructmaven.entity.Foo;
import com.example.springmapstructmaven.dto.FooDto;

import javax.annotation.processing.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor"
)
public class FooMapperImpl implements IFooMapper {

    @Override
    public Foo fooDtoToFoo(FooDto dto) {
        if ( dto == null ) {
            return null;
        }

        Foo foo = new Foo();

        foo.setBar( dto.getBr() );
        foo.setBaz( String.valueOf( dto.getBz() ) );

        return foo;
    }
    @Override
    public FooDto fooToFooDto(Foo foo) {
        if ( foo == null ) {
            return null;
        }

        FooDto fooDto = new FooDto();

        fooDto.setBr( foo.getBar() );
        if ( foo.getBaz() != null ) {
            fooDto.setBz( Integer.parseInt( foo.getBaz() ) );
        }

        return fooDto;
    }
}
