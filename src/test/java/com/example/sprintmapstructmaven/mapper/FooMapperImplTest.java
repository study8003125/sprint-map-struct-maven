package com.example.sprintmapstructmaven.mapper;

import com.example.springmapstructmaven.dto.FooDto;
import com.example.springmapstructmaven.entity.Foo;
import com.example.springmapstructmaven.mapper.IFooMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FooMapperImplTest {
    @Test
    public void shouldMapFooDtoToFoo() {
        // given
        FooDto fooDto = new FooDto();
        fooDto.setBr("BAR");
        fooDto.setBz(8888);
        // when
        Foo foo = IFooMapper.INSTANCE.fooDtoToFoo(fooDto);
        // then
        assertEquals("BAR", foo.getBar());
        assertEquals("8888", foo.getBaz());
    }

    @Test
    public void shouldMapFooToFooDto() {
        // given
        Foo foo = new Foo();
        foo.setBar("BAR");
        foo.setBaz("8888");
        // when
        FooDto fooDto = IFooMapper.INSTANCE.fooToFooDto(foo);
        // then
        assertEquals("BAR", fooDto.getBr());
        assertEquals(8888, fooDto.getBz());
    }
}